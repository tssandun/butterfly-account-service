package com.auxilium;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigurationFactory {

	private String databaseHost;
	private int databasePort;
	private String serviceHost;
	private int servicePort;
	private String accountsDatabase;
	private String accountsAdmin;
	private String accountsPassword;
	
	public ConfigurationFactory() {
		
		Properties properties = new Properties();
		InputStream config = null;

		try {
			config = ConfigurationFactory.class.getResourceAsStream("config.properties");

			// load a properties file
			properties.load(config);

			// get the property values
			setDatabaseHost(properties.getProperty("database.host"));
			setServiceHost(properties.getProperty("service.host"));
			setDatabasePort(Integer.parseInt(properties.getProperty("database.port")));
			setServicePort(Integer.parseInt(properties.getProperty("service.port")));
			setAccountsDatabase(properties.getProperty("database.accounts"));
			setAccountsAdmin(properties.getProperty("database.accounts.admin"));
			setAccountsPassword(properties.getProperty("database.accounts.password"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (NumberFormatException ex){
			ex.printStackTrace();
		}finally {
			if (config != null) {
				try {
					config.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getDatabaseHost() {
		return databaseHost;
	}

	private void setDatabaseHost(String host) {
		this.databaseHost = host;
	}

	public String getServiceHost() {
		return serviceHost;
	}

	private void setServiceHost(String host) {
		this.serviceHost = host;
	}

	public int getDatabasePort() {
		return databasePort;
	}

	public void setDatabasePort(int port) {
		this.databasePort = port;
	}

	public int getServicePort() {
		return servicePort;
	}

	public void setServicePort(int port) {
		this.servicePort = port;
	}

	public String getAccountsDatabase() {
		return accountsDatabase;
	}

	private void setAccountsDatabase(String accountsDatabase) {
		this.accountsDatabase = accountsDatabase;
	}

	public String getAccountsAdmin() {
		return accountsAdmin;
	}

	private void setAccountsAdmin(String accountsUser) {
		this.accountsAdmin = accountsUser;
	}

	public String getAccountsPassword() {
		return accountsPassword;
	}

	private void setAccountsPassword(String accountsPassword) {
		this.accountsPassword = accountsPassword;
	}
}
