package com.auxilium;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;

import static com.mongodb.client.model.Filters.*;

import java.util.UUID;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONObject;

@Path("/account")
public class AccountService {
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response viewAccount(@PathParam("id") String id) {
    	
    	ConfigurationFactory configFac = new ConfigurationFactory();

    	String accountsAdmin = configFac.getAccountsAdmin();
    	String accountsDatabase = configFac.getAccountsDatabase();
    	String accountsPassword = configFac.getAccountsPassword();
    	String databaseHost = configFac.getDatabaseHost();
    	int databasePort = configFac.getDatabasePort();

    	//Authenticating to database
    	DatabaseClientFactory mongoClientFactory = new DatabaseClientFactory(accountsAdmin,accountsDatabase,accountsPassword,databaseHost,databasePort);
    	//Initializing database client
    	MongoClient mongoClient = mongoClientFactory.getMongoClient();
    	
    	//Connecting to database
    	MongoDatabase db = mongoClient.getDatabase("accounts");    	
    	MongoCollection<Document> collection = db.getCollection("users");
    	
    	//Initialize response object
    	JSONObject response = new JSONObject();
    	JSONObject dbObject = new JSONObject();
    	//Exception flag
    	Boolean exFlag = false;
    	
    	try {
    		//Retrieve data from database
        	ObjectId objId = new ObjectId(id);
    		Document doc = collection.find(eq("_id", objId)).first();
    		doc.remove("password");
    		
    		//Convert _id object to hexadecimal string
    		Object idObj = doc.get("_id");
            String idStr = (String)idObj.toString();
            
            doc.remove("_id");
            doc.put("id", idStr);
    		    		
        	dbObject = new JSONObject(doc.toJson());
    	}catch(NullPointerException ex) {
    		//Generating User does not exist error
    		String error = "User does not exist!";
    		response.put("message", error);
    		response.put("status", "failed");
    		exFlag = true;
    	}catch(java.lang.IllegalArgumentException ex){
    		//Generating Invalid request error
    		String error = "Invalid request!";
    		response.put("message", error);
    		response.put("status", "failed");
    		exFlag = true;
    	}
    	
    	//close database client
    	mongoClient.close();
    	
    	//Send response upon failure
    	if(exFlag) {
    		return Response.status(400).header("location", "account/" + id).entity(response.toString()).build();
    	}

    	//Build response
		response.put("message", dbObject);
		response.put("status", "success");
		//Send response upon success
		return Response.status(200).header("location", "account/" + id).entity(response.toString()).build();
    }
    
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAccount(String request) {
    	
    	ConfigurationFactory configFac = new ConfigurationFactory();

    	String accountsAdmin = configFac.getAccountsAdmin();
    	String accountsDatabase = configFac.getAccountsDatabase();
    	String accountsPassword = configFac.getAccountsPassword();
    	String databaseHost = configFac.getDatabaseHost();
    	int databasePort = configFac.getDatabasePort();
    	String serviceHost = configFac.getServiceHost();
    	int servicePort = configFac.getServicePort();

    	//Parse request as json object
    	JSONObject requestObj = new JSONObject(request);

    	//Initializing response object
		JSONObject response = new JSONObject();
    	JSONObject dbObject = new JSONObject();    	
		String email = null;
		String fname = null;
		String lname = null;
		String alias = null;
		String password = null;

		//Check if all the required fields are there.
		//If not response with an error
    	try {
    		email = requestObj.getString("email");
    		fname = requestObj.getString("fname");
    		lname = requestObj.getString("lname");
    		alias = requestObj.getString("alias");
    		password = requestObj.getString("password");
    	}catch(org.json.JSONException ex) {
    		String error = ex.getLocalizedMessage().replace("JSONObject[\"", "'").replaceAll("\"]", "'");
    		response.put("message", error);
    		response.put("status", "failed");
			return Response.status(400).header("location", "account/").entity(response.toString()).build();
    	}
    	
    	//Validate email and password
    	ValidationService validator = new ValidationService();
    	Boolean validEmail = validator.validateEmail(email);
    	Boolean validPassword = validator.validatePassword(password);
    	
    	if(!validEmail) {
    		String error = "Invalid E-mail address";
    		response.put("message", error);
    		response.put("status", "failed");
			return Response.status(400).header("location", "account/").entity(response.toString()).build();
    	}else if(!validPassword){
    		String error = "Invalid password";
    		response.put("message", error);
    		response.put("status", "failed");
			return Response.status(400).header("location", "account/").entity(response.toString()).build();
    	}
    	
    	//Authenticating to database
    	DatabaseClientFactory mongoClientFactory = new DatabaseClientFactory(accountsAdmin,accountsDatabase,accountsPassword,databaseHost,databasePort);
    	//Initializing database client
    	MongoClient mongoClient = mongoClientFactory.getMongoClient();
    	
    	//Connecting to database
    	MongoDatabase db = mongoClient.getDatabase("accounts");    	
    	MongoCollection<Document> collection = db.getCollection("users");

    	//check duplicate alias
		Boolean aliasCheck = collection.find(eq("alias", alias)).first() == null;
		//check duplicate email
		BasicDBObject query = new BasicDBObject("email.address", email);
		FindIterable<Document> result = collection.find(query);
		Boolean emailCheck = result.first() == null;
		
		if(aliasCheck && emailCheck) {
			//Generate dbObject to be persist to the database
			Long timestamp = System.currentTimeMillis();
			String uniqueID = UUID.randomUUID().toString();
    		JSONObject emailObj = new JSONObject("{address:" + email + ",status:\"unconfirmed\",timestamp:\"" + timestamp +"\",uuid:\"" + uniqueID + "\"}");
    		
    		//Request body is not directly saved to database
    		//in case of requests containing unnecessary fields
    		dbObject.put("email", emailObj);
    		dbObject.put("fname", fname);
    		dbObject.put("lname", lname);
    		dbObject.put("alias", alias);
    		dbObject.put("password", password);

    		Document doc = Document.parse(dbObject.toString());
    	
    		//Persist to database
    		collection.insertOne(doc);
    		
    		//Get the persisted record from the database
    		Document dbOutDoc = collection.find(eq("alias", alias)).first();
    		
    		if(dbOutDoc == null) {
        		String error = "Database error. Could not persist to database!";
        		response.put("message", error);
        		response.put("status", "failed");
    			return Response.status(500).header("location", "account/").entity(response.toString()).build();
    		}

    		//Convert _id object to hexadecimal string
    		Object idObj = dbOutDoc.get("_id");
            String idStr = (String)idObj.toString();
            dbOutDoc.remove("_id");
            dbOutDoc.put("id", idStr);
            
    		//Convert output to json object
    		JSONObject dbOutJson = new JSONObject(dbOutDoc);
    		
    		//close database connection
    		mongoClient.close();
    	
    		//Remove password from the dbOut before adding to response
    		dbOutJson.remove("password");

    		//Generate response object
    		response.put("message", dbOutJson);
    		response.put("status", "success");
    		
    		//Send validation email    		
    		HTMLEmailClient emailClient = new HTMLEmailClient("testhtml15258@gmail.com",email);
    		
    		String content = "<a href=\"http://" + serviceHost + ":" + servicePort + "/butterfly-account-service/account/confirm/" + idStr + "/" + uniqueID + "\">Confirm email</a>";
    		
    		emailClient.sendHTMLEmail(content);
    		
    		//Send response upon success
			return Response.status(201).header("location", "account/").entity(response.toString()).build();
			
		}else {
			String error = null;
			//Generate error message according to duplicate entry
			if(!emailCheck && aliasCheck) {
				error = "Email address \'" + email + "\' already in use.";
			}else if(!aliasCheck && emailCheck) {
				error = "alias \'" + alias + "\' already in use.";
			}else {
				error = "alias \'" + alias + "\' and Email address \'" + email + "\' already in use.";
			}
			
			//Close database connection
			mongoClient.close();

			//Build response
			response.put("message", error);
			response.put("status", "failed");
			//Send response upon failure
			return Response.status(409).header("location", "account/").entity(response.toString()).build();
		}
    }
  
    @GET
    @Path("/confirm/{id}/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmEmail(@PathParam("id") String id, @PathParam("uuid") String uuid) {
    	
    	ConfigurationFactory configFac = new ConfigurationFactory();

    	String accountsAdmin = configFac.getAccountsAdmin();
    	String accountsDatabase = configFac.getAccountsDatabase();
    	String accountsPassword = configFac.getAccountsPassword();
    	String databaseHost = configFac.getDatabaseHost();
    	int databasePort = configFac.getDatabasePort();

    	//Authenticating to database
    	DatabaseClientFactory mongoClientFactory = new DatabaseClientFactory(accountsAdmin,accountsDatabase,accountsPassword,databaseHost,databasePort);
    	//Initializing database client
    	MongoClient mongoClient = mongoClientFactory.getMongoClient();
    	
    	//Connecting to database
    	MongoDatabase db = mongoClient.getDatabase("accounts");    	
    	MongoCollection<Document> collection = db.getCollection("users");
    	
    	//Initialize response object
    	JSONObject response = new JSONObject();
    	String uniqueId = null;
    	
    	//Exception flag
    	Boolean exFlag = false;
    	
    	try {
    		//Retrieve data from database
        	ObjectId objId = new ObjectId(id);
    		Document doc = collection.find(eq("_id", objId)).first();
    		
    		//Get uuid from the database
    		JSONObject dbObj = new JSONObject(doc.toJson());
    		JSONObject email = new JSONObject(dbObj.get("email").toString());
    		uniqueId = email.get("uuid").toString();
    		System.out.println(uniqueId);
    		System.out.println(uuid);
    	
    	}catch(NullPointerException ex) {
    		//Generating User does not exist error
    		String error = "User does not exist!";
    		response.put("message", error);
    		response.put("status", "failed");
    		exFlag = true;
    	}catch(java.lang.IllegalArgumentException ex){
    		//Generating Invalid request error
    		String error = "Invalid request!";
    		response.put("message", error);
    		response.put("status", "failed");
    		exFlag = true;
    	}
    	
    	//Send response upon failure
    	if(exFlag) {
    		return Response.status(400).header("location", "account/" + id).entity(response.toString()).build();
    	}

    	if(uuid.equals(uniqueId)) {
    		//update status
        	ObjectId objId = new ObjectId(id);
    		collection.updateOne(eq("_id", objId), new Document("$set", new Document("email.status", "confirmed")));
    		collection.updateOne(eq("_id", objId), new Document("$unset", new Document("email.uuid", "")));

        	//close database client
        	mongoClient.close();
        	
    		//Build response
    		String message = "Email verification successfull";
    		response.put("message", message);
    		response.put("status", "success");
    		//Send response upon success
    		return Response.status(200).header("location", "account/" + id).entity(response.toString()).build();
    	}else {
        	//close database client
        	mongoClient.close();
    		//Build response upon success
    		String message = "Email verification failed. Invalid verification link.";
    		response.put("message", message);
    		response.put("status", "failed");
    		//Send response upon failure
    		return Response.status(400).header("location", "account/" + id).entity(response.toString()).build();
    	}
    
    }
    
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAccount(@PathParam("id") String id, String request) {
    	
    	Account acc = new Account();
    	
    	//Parse request as json object
    	JSONObject requestObj = new JSONObject(request);

    	//Initializing response object
		JSONObject response = new JSONObject();
 	
		String email = null;
		String fname = "";
		String lname = null;
		String alias = null;
		String password = null;

		//Check if all the required fields are there.
		//If not response with an error
    	try {
    		email = requestObj.getString("email");
    		fname = requestObj.getString("fname");
    		lname = requestObj.getString("lname");
    		alias = requestObj.getString("alias");
    		password = requestObj.getString("password");
    	}catch(org.json.JSONException ex) {
    		String error = ex.getLocalizedMessage().replace("JSONObject[\"", "'").replaceAll("\"]", "'");
    		response.put("message", error);
    		response.put("status", "failed");
			return Response.status(400).header("location", "account/").entity(response.toString()).build();
    	}
    	
    	//Validate email and password
    	ValidationService validator = new ValidationService();
    	Boolean validEmail = validator.validateEmail(email);
    	Boolean validPassword = validator.validatePassword(password);
    	
    	if(!validEmail) {
    		String error = "Invalid E-mail address";
    		response.put("message", error);
    		response.put("status", "failed");
			return Response.status(400).header("location", "account/").entity(response.toString()).build();
    	}else if(!validPassword){
    		String error = "Invalid password";
    		response.put("message", error);
    		response.put("status", "failed");
			return Response.status(400).header("location", "account/").entity(response.toString()).build();
    	}
    	
    	acc.updateAccount(id, fname, lname, alias, email, password);
    	   	
    	response.put("status", "success");
    	//Send response upon success
		return Response.status(201).header("location", "account/").entity(response.toString()).build();
    	
    }
    
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccount(@PathParam("id") String id) {
    	
    	Account acc = new Account();
    	return acc.deleteAccount(id);
    	
    }
    
}
