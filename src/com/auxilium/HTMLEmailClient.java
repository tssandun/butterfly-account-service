package com.auxilium;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class HTMLEmailClient {

	final String username = "htmltest15258@gmail.com";
	final String password = "htmltestpwrd";
	
	private String to;
	private String from;
	private Session session;
	
	public HTMLEmailClient(String sender, String recipient) {
		this.to = recipient;
		this.from = sender;
		Properties properties = new Properties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.port", "465");
		properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		properties.setProperty("mail.smtp.socketFactory.fallback", "false");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "465");
		properties.put("mail.debug", "true");
		properties.put("mail.store.protocol", "pop3");
	    properties.put("mail.transport.protocol", "smtp");
		
		this.session = Session.getInstance(properties,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
	}
	
	public void sendHTMLEmail(String content) {

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(this.from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(this.to));
			message.setSubject("Testing Subject");
			message.setContent(content, "text/html");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		
	}
}
