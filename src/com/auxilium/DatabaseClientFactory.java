package com.auxilium;

import java.util.Arrays;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class DatabaseClientFactory {

	private MongoClient mongoClient;
	
	public DatabaseClientFactory(String username,String dbname, String pwrd, String host, int port) {

    	//Creating database credentials
    	MongoCredential credential = MongoCredential.createCredential(username, dbname, pwrd.toCharArray());
    	//Creating database client with credentials
    	MongoClient mongoClient = new MongoClient(new ServerAddress(host, port),
    	                                         Arrays.asList(credential));
    	this.setMongoClient(mongoClient);
	}

	public MongoClient getMongoClient() {
		return mongoClient;
	}

	private void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}
}
