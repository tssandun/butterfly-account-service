package com.auxilium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationService {

	private Pattern emailPattern;
	private Pattern pwrdPattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//Minimum 6 Maximum 20 characters 
	//At least 1 upper or lower case letter
	//At least 1 Number 	
	//Optional Special Chars
	private static final String PASSWORD_PATTERN = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d!$%@#��*?&]{6,20}$";

	public ValidationService() {
		emailPattern = Pattern.compile(EMAIL_PATTERN);
		pwrdPattern = Pattern.compile(PASSWORD_PATTERN);
	}

	/**
	 * Validate email with regular expression
	 *
	 * @param email
	 *            email for validation
	 * @return true valid email, false invalid hex
	 */
	public Boolean validateEmail(final String email) {

		matcher = emailPattern.matcher(email);
		return matcher.matches();

	}

	public Boolean validatePassword(final String password) {

		matcher = pwrdPattern.matcher(password);
		return matcher.matches();

	}
}
