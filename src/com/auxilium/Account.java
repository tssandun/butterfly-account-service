package com.auxilium;

import static com.mongodb.client.model.Filters.eq;

import javax.ws.rs.core.Response;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;

public class Account {
	
	public void updateAccount(String id, String fname, String lname, String alias, String email, String password) {
		
    	ConfigurationFactory configFac = new ConfigurationFactory();

    	String accountsAdmin = configFac.getAccountsAdmin();
    	String accountsDatabase = configFac.getAccountsDatabase();
    	String accountsPassword = configFac.getAccountsPassword();
    	String databaseHost = configFac.getDatabaseHost();
    	int databasePort = configFac.getDatabasePort();

    	//Authenticating to database
    	DatabaseClientFactory mongoClientFactory = new DatabaseClientFactory(accountsAdmin,accountsDatabase,accountsPassword,databaseHost,databasePort);
    	//Initializing database client
    	MongoClient mongoClient = mongoClientFactory.getMongoClient();
    	
    	//Connecting to database
    	MongoDatabase db = mongoClient.getDatabase("accounts");    	
    	MongoCollection<Document> collection = db.getCollection("users");
    	
    	BasicDBObject doc = new BasicDBObject();
/*    	doc.append("$set", new BasicDBObject().append("fname", fname));
    	doc.append("$set", new BasicDBObject().append("lname", lname));
    	doc.append("$set", new BasicDBObject().append("alias", alias));
    	doc.append("$set", new BasicDBObject().append("alias", alias));*/
    	doc.append("fname", fname);
    	doc.append("lname", lname);
    	doc.append("alias", alias);
    	doc.append("email", email);
    	doc.append("password", password);
    	BasicDBObject setQuery = new BasicDBObject();
    	setQuery.append("$set", doc);
    	
    	ObjectId objId = new ObjectId(id);
    	collection.updateOne(new BasicDBObject("_id", objId),setQuery);
/*    	collection.updateOne(
    		    new BasicDBObject("_id", objId),
    		    new BasicDBObject("$set", new BasicDBObject("fname", fname))
    		);*/
    	
	}
	
	public Response deleteAccount(String id) {
		
    	ConfigurationFactory configFac = new ConfigurationFactory();

    	String accountsAdmin = configFac.getAccountsAdmin();
    	String accountsDatabase = configFac.getAccountsDatabase();
    	String accountsPassword = configFac.getAccountsPassword();
    	String databaseHost = configFac.getDatabaseHost();
    	int databasePort = configFac.getDatabasePort();

    	//Authenticating to database
    	DatabaseClientFactory mongoClientFactory = new DatabaseClientFactory(accountsAdmin,accountsDatabase,accountsPassword,databaseHost,databasePort);
    	//Initializing database client
    	MongoClient mongoClient = mongoClientFactory.getMongoClient();
    	
    	//Connecting to database
    	MongoDatabase db = mongoClient.getDatabase("accounts");    	
    	MongoCollection<Document> collection = db.getCollection("users");
    	
    	
		try {
			
			ObjectId objId = new ObjectId(id);
			collection.deleteOne(new BasicDBObject("_id", objId));
			
		} catch (java.lang.IllegalArgumentException e) {

			return Response.status(400).build();
		}
		
		return Response.status(204).build();
		
		
	}

}
